# Architecture Ref. Card 03

## Inbetriebnahme auf eigenem Computer

Zuerst braucht man das Repository zu forken

Klonen mit:
```sh
git clone https://gitlab.com/bbwrl/m346-ref-card-03.git
cd m346-ref-card-03
```
Projekt mit Maven builden
```sh
mvn package
```

## Gitlab-Runner

Bei GitLab findet man unter Settings -> CI/CD -> Runners. Dort klickt man dann auf "New project runner". Als Operating system sollte Linux gewählt werden und der Haken sollte bei "Run untagged jobs" gesetzt sein.

Docker muss nun gestartet werden und die folgende drei Befehle müssen ausgeführt werden:

```sh
docker run -d --name gl-runner -v ∕var∕run∕docker.sock:∕var∕run∕docker.sock gitlab∕gitlab-runner
```
```sh
docker exec gl-runner gitlab-runner register --url https:∕∕gitlab.com --token HIER_IHREN_TOKEN_EINFUEGEN --executor docker --docker-image docker:23.0.4 --docker-privileged --non-interactive
```
```sh
docker restart gl-runner
```

## Build und Deploy

Um eine Pipeline zu erstellen braucht man ein ".gitlab-ci.yml" file. Dieses ist bereits im Repository vorgegeben. 

### AWS
Erstelle nun in AWS ein ECR Repository und in ECS einen Cluster, einen Service und falls noch nicht vorhanden eine Taskdefinition.   gehe dann zu Settings -> CI/CD -> Variables -> Expand. Füge dort folgende Variabeln hinzu:

- AWS_ACCESS_KEY_ID
- AWS_DEFAULT_REGION
- AWS_SECRET_ACCESS_KEY
- AWS_SESSION_TOKEN
- CI_AWS_ECR_REGISTRY
- CI_AWS_ECR_REPOSITORY_NAME
- CI_AWS_ECS_CLUSTER_NAME
- CI_AWS_ECS_SERVICE_NAME

Die dazugehörigen Werte findet man im Lerner Lab unter "AWS Details". Die Werte für die Befehle, welche mit "CI" starten, findet man in ECR oder ECS. Wichtig bei "CI_AWS_ECR_REGISTRY" ist, dass man die URL nur bis zum ".com" kopiert und den Rest weglässt.

Nun kann die Pipeline gestartet werden. Gehe dafür zu Build -> Pipelines und klicke auf "Run pipeline". Die Werte kann man so belassen und nochmal auf "Run pipeline" klicken. Diese sollte dann erfolgreich sein.


